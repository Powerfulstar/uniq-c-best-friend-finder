﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookFan
{
     public class From
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Datum2
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class To
    {
        public IList<Datum2> data { get; set; }
    }

    
  
    public class Action
    {
        public string name { get; set; }
        public string link { get; set; }
    }

    public class Privacy
    {
        public string value { get; set; }
        public string description { get; set; }
        public string friends { get; set; }
        public string allow { get; set; }
        public string deny { get; set; }
    }

    public class Datum3
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Cursors
    {
        public string after { get; set; }
        public string before { get; set; }
    }

    public class Paging
    {
        public Cursors cursors { get; set; }
        public string next { get; set; }
    }

   

     

    public class Datum4
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class WithTags
    {
        public IList<Datum4> data { get; set; }
    }

    public class Location
    {
        public string city { get; set; }
        public string country { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string street { get; set; }
    }

    public class Place
    {
        public string id { get; set; }
        public string name { get; set; }
        public Location location { get; set; }
    }

    public class From2
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class MessageTag
    {
        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int offset { get; set; }
        public int length { get; set; }
    }

    public class Datum5
    {
        public string id { get; set; }
        public From2 from { get; set; }
        public string message { get; set; }
        public bool can_remove { get; set; }
        public DateTime created_time { get; set; }
        public int like_count { get; set; }
        public bool user_likes { get; set; }
        public IList<MessageTag> message_tags { get; set; }
    }

    public class Cursors2
    {
        public string after { get; set; }
        public string before { get; set; }
    }

    public class Paging2
    {
        public Cursors2 cursors { get; set; }
    }

    public class Comments
    {
        public IList<Datum5> data { get; set; }
        public Paging2 paging { get; set; }
    }

    public class Shares
    {
        public int count { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public From from { get; set; }
        public To to { get; set; }
        public string message { get; set; }
         
        public string picture { get; set; }
        public string link { get; set; }
        public string name { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
        public IList<Action> actions { get; set; }
        public Privacy privacy { get; set; }
        public string type { get; set; }
        public string status_type { get; set; }
        public DateTime created_time { get; set; }
        public DateTime updated_time { get; set; }
        public bool subscribed { get; set; }
        public Likes likes { get; set; }
        public string story { get; set; }
         
        public string object_id { get; set; }
        public WithTags with_tags { get; set; }
        public Place place { get; set; }
        public Comments comments { get; set; }
        public string source { get; set; }
        public Shares shares { get; set; }
    }

    public class Paging3
    {
        public string previous { get; set; }
        public string next { get; set; }
    }

    public class Posts
    {
        public IList<Datum> data { get; set; }
        public Paging3 paging { get; set; }
    }

    public class Example
    {
        public Posts posts { get; set; }
        public string id { get; set; }
    }
    public class Likes
    {
        public IList<Datum3> data { get; set; }
        public Paging paging { get; set; }
    }
    public class FriendList
    {
        public string name { get; set; }
        public string id { get; set; }
        public int TotalLikes { get; set; }
        public int TotalPosts { get; set; }
    }
    public class LikersList
    {
        public IList<Datum3> Likers { get; set; }
        public string Status { get; set; }
    }
}
