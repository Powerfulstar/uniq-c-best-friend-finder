﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FacebookFan
{
    class Program
    {
        public static string token = "CAACEdEose0cBAOK1c6EwOFc1lPWTjg4bTCAyWaSxpLDzZCCxOvB6JLFsxtDxpD5AP4GNXiZCpEDegqTkkx82hBm551IX36fvqLxd6Y6IWCdQrVUA3Q6JEhbpd7xVnFo0CSw95SRZAMbS3dmuumrCQqMEScz1qeR3ZCniuFQZA4DKbWnuiJpMvYgZCRqGGdZBCFOI6JwkHnPlrc4Ire0p5Br";
        public static string Graph_URL = @"https://graph.facebook.com/";
        public static string Access_Token = @"&access_token="+token;
        public static string My_Post_URL = Graph_URL + @"me?fields=posts" + Access_Token;
        
         
        

        static void Main(string[] args)
        {
            string BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string friendListFilePath = BaseDirectory + @"/UniqCFriends.txt";
            string newfriendListFilePath = BaseDirectory + @"/BestFriends.txt";
            string AllPostsWitheLikersFileath = BaseDirectory + @"/likers.txt";
            List<FriendList> FriendList = JsonConvert.DeserializeObject<List<FriendList>>(File.ReadAllText(friendListFilePath));            
            Example each = new Example();
            WebClient client = new WebClient();
            List<LikersList> likerList = new List<LikersList>();

            int i = 0;
            string DownLoadedJson = client.DownloadString(My_Post_URL);
            JObject o = JObject.Parse(DownLoadedJson);
            Console.WriteLine(o["id"]);
            foreach (var item in o["posts"]["data"])
            {
                i += 1;
                Console.WriteLine(i + ":\t\t" + item["message"] + "\n\n");
                LikersList likers = new LikersList();
                likers.Status = (string)item["message"];
                string ID = (string)item["id"];
                string LikeURL = Graph_URL + ID + @"/likes?limit=500" + Access_Token;

                likers.Likers = JsonConvert.DeserializeObject<Likes>(client.DownloadString(LikeURL)).data;
                likerList.Add(likers);
            }
            My_Post_URL = (string)o["posts"]["paging"]["next"];
            bool flag = true;
            while (flag)
            {
                try
                {
                    DownLoadedJson = client.DownloadString(My_Post_URL);
                    o = JObject.Parse(DownLoadedJson);
                    My_Post_URL = (string)o["paging"]["next"];
                    if (o == null)
                    {
                        flag = false;
                    }
                    foreach (var item in o["data"])
                    {
                        i += 1;
                        Console.WriteLine(i + ":\t\t" + item["message"] + "\n\n");
                        LikersList likers = new LikersList();
                        likers.Status = (string)item["message"];
                        string ID = (string)item["id"];
                        string LikeURL = Graph_URL + ID + @"/likes?limit=500" + Access_Token;

                        likers.Likers = JsonConvert.DeserializeObject<Likes>(client.DownloadString(LikeURL)).data;
                        likerList.Add(likers);
                    }
                }
                catch (Exception)
                {
                    break;
                }
            }
            File.WriteAllText(AllPostsWitheLikersFileath, JsonConvert.SerializeObject(likerList));

            List<FriendList> friendList = JsonConvert.DeserializeObject<List<FriendList>>(File.ReadAllText(friendListFilePath));
            likerList = JsonConvert.DeserializeObject<List<LikersList>>(File.ReadAllText(AllPostsWitheLikersFileath));
            foreach (var item in friendList)
            {
                foreach (var likes in likerList)
                {
                    foreach (var like in likes.Likers)
                    {
                        if (like.id == item.id)
                        {
                            item.TotalLikes += 1;                            
                        }
                    }
                    item.TotalPosts += 1;
                }
            }
            File.WriteAllText(newfriendListFilePath, JsonConvert.SerializeObject(friendList.OrderBy(ob => ob.TotalLikes).Reverse(), Formatting.Indented));
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
